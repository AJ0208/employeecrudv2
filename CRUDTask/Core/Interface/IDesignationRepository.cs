﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Core.Models.DesigationsListParameter;
using static Core.Models.DesignationListById;
using static Core.Models.EmployeeListParameter;

namespace Core.Interface
{
    public interface IDesignationRepository
    {
        Task<Response> AddAsync(DtoAddDesignation  dtoAddDesignation);
        Task<Response> UpdateAsync(DtoUpdateDesignation dtoUpdateDesignation);
    
    

        Task<DesignationResponseList> GetAllEmployeesListByDesignationAsync(DesignationListById designationListById);

        Task<AllDesigationsResponseList> GetAllDesignationsListAsync(DesigationsListParameter desigationsListParameter);
    }
}
