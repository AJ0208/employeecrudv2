﻿
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Core.Models.EmployeeListById;
using static Core.Models.EmployeeListParameter;

namespace Core.Interface
{
    public interface IEmployeeRepository
    {
        Task<Response> AddAsync(DtoAddEmployeeData dtoAddEmployeeData);
        Task<Response> UpdateAsync(DtoUpdateEmployeeData dtoUpdateEmployeeData);
        Task<Response> DeleteAsync(DtoDeleteEmployeeData dtoDeleteEmployeeData);

        Task<EmployeeResponseList> GetEmployeeListByIdAsync(EmployeeListById employeeListById);
        Task<ResponseList> GetAllEmployeeListAsync(EmployeeListParameter employeeListParameter);
        // Task<Response<DtoAddEmployeeData>> GetSingleRecord(string id);

    }
}
