﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DesignationListById
    {
        [Required]
        public Guid Id { get; set; }

        public class DesignationResponseList
        {
            public string? Message { get; set; }

            public int TotalRecords { get; set; }

            public List<EmployeeList>? Data { get; set; }

            public bool Status { get; set; }

        }

        public class EmployeeList
        {
          
            public Guid? Id { get; set; }

            public string? EmployeeName { get; set; }


            public string? DesignationName { get; set; }


            public double Salary { get; set; }


            public string? City { get; set; }


            public DateTime? CreatedOn { get; set; }
            public Guid? CreatedBy { get; set; }

            public DateTime? UpdatedOn { get; set; }
            public Guid? UpdatedBy { get; set; }

            public DateTime? DeletedOn { get; set; }
            public Guid? DeletedBy { get; set; }


        }
    }

}
