﻿  using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class EmployeeListParameter
    {
        public int Start { get; set; }

        public int PageSize { get; set; }

        public string? SortCol { get; set; }

        public string? SearchKey { get; set; }

        public class ResponseList
        {
            public string? Message { get; set; }

            public int TotalRecords { get; set; }

            public List<EmployeesList>? Data { get; set; }

            public string? Status { get; set; }

        }

        public class EmployeesList
        {
            
            public Guid ID { get; set; }


            public string? EmployeeName { get; set; }


            public string? DesignationName { get; set; }

            public double Salary { get; set; }


            public string? City { get; set; }

            public DateTime? CreatedOn { get; set; }
            public Guid? CreatedBy { get; set; }

            public DateTime? UpdatedOn { get; set; }
            public Guid? UpdatedBy { get; set; }

            public DateTime? DeletedOn { get; set; }
            public Guid? DeletedBy { get; set; }


        }
    }

}

