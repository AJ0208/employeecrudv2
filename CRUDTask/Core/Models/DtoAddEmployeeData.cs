﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DtoAddEmployeeData
    {
        [Required]
        public Guid DesignationId { get; set; }

        [Required, MaxLength(100, ErrorMessage = "You have exceeded the character length 100."), RegularExpression(@"^[\p{L} \.\-]+$")]
        public string? Name { get; set; }


        [Required]
        public double Salary { get; set; }


        [MaxLength(100, ErrorMessage = "You have exceeded the character length 100."), RegularExpression(@"^[\p{L} \.\-]+$")]
        public string? City { get; set; }

  
         public Guid CreatedBy { get; set; }


    }
}
