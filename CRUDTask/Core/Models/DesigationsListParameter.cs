﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DesigationsListParameter
    {
        public int Start { get; set; }

        public int PageSize { get; set; }

        public string? SortCol { get; set; }

        public string? SearchKey { get; set; }

        public class AllDesigationsResponseList
        {
            public string? Message { get; set; }

            public int TotalRecords { get; set; }

            public List<AllDesigationsList>? Data { get; set; }

            public string? Status { get; set; }

        }

        public class AllDesigationsList
        {

            public Guid Id { get; set; }
            public string? Name { get; set; }
     

        }
    }

}


