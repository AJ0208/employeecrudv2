﻿
using Core.Interface;
using Core.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Core.Models.DesignationListById;
using static Core.Models.EmployeeListById;
using static Core.Models.EmployeeListParameter;
using Response = Core.Models.Response;

namespace Infrastructure
{
    public class EmpoyeeRepository : IEmployeeRepository
    {
        public readonly IConfiguration _configuration;

        private static string? connectionString = string.Empty;

        public EmpoyeeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration["ConnectionStrings:EmployeeData"];
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public async Task<Response> AddAsync(DtoAddEmployeeData dtoAddEmployeeData)
        {
            Response response;

            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspAddEmployeeData]", dtoAddEmployeeData, commandType: CommandType.StoredProcedure);

            return response;

        }

        public async Task<Response> UpdateAsync(DtoUpdateEmployeeData dtoUpdateEmployeeData)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspUpdateEmployeeData]", dtoUpdateEmployeeData, commandType: CommandType.StoredProcedure);
            return response;
        }

        public async Task<Response> DeleteAsync(DtoDeleteEmployeeData dtoDeleteEmployeeData)
        {         
                Response response;

                using IDbConnection db = Connection;
                response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspDeleteEmployeeData]", dtoDeleteEmployeeData , commandType: CommandType.StoredProcedure);
                return response;

        }



        public async Task<EmployeeResponseList> GetEmployeeListByIdAsync(EmployeeListById employeeListById)
        {

            EmployeeResponseList response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[uspGetEmployeeListById]", employeeListById, commandType: CommandType.StoredProcedure);


            response = result.Read<EmployeeResponseList>().FirstOrDefault()!;
            response.Data = result.Read<EmployeeListId>().ToList();
            response.TotalRecords = result.Read<int>().FirstOrDefault();
            return response;

        }


        public async Task<ResponseList> GetAllEmployeeListAsync(EmployeeListParameter employeeListParameter)
        {
            ResponseList response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[uspGetAllEmployeesList]", employeeListParameter, commandType: CommandType.StoredProcedure);


            response = result.Read<ResponseList>().FirstOrDefault()!;
            response.Data = result.Read<EmployeesList>().ToList();
            response.TotalRecords = result.Read<int>().FirstOrDefault();
            return response;


        }


    }


}




