﻿using Core.Interface;
using Core.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Core.Models.DesigationsListParameter;
using static Core.Models.DesignationListById;
using static Core.Models.EmployeeListParameter;

namespace Infrastructure
{
    public class DesignationRepository : IDesignationRepository
    {

        public readonly IConfiguration _configuration;

        private static string? connectionString = string.Empty;

        public DesignationRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration["ConnectionStrings:EmployeeData"];
        }

       
        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }


        public async Task<Response> AddAsync(DtoAddDesignation  dtoAddDesignation)
        {
            Response response;

            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspAddDesignation]", dtoAddDesignation, commandType: CommandType.StoredProcedure);

            return response;

        }

        public async Task<Response> UpdateAsync(DtoUpdateDesignation dtoUpdateDesignation)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspUpdateDesignation]", dtoUpdateDesignation, commandType: CommandType.StoredProcedure);
            return response;
        }


        public async Task<DesignationResponseList> GetAllEmployeesListByDesignationAsync(DesignationListById  designationListById)
        {

            DesignationResponseList response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[uspGetAllEmployeesListByDesignationId]", designationListById, commandType: CommandType.StoredProcedure);


            response = result.Read<DesignationResponseList>().FirstOrDefault()!;
            response.Data = result.Read<EmployeeList>().ToList();
            response.TotalRecords = result.Read<int>().FirstOrDefault();
            return response;

        }

       
        public async Task<AllDesigationsResponseList> GetAllDesignationsListAsync(DesigationsListParameter desigationsListParameter)
        {
            AllDesigationsResponseList response;
            using IDbConnection db = Connection;
            var result = await db.QueryMultipleAsync("[uspGetAllDesignationsList]", desigationsListParameter, commandType: CommandType.StoredProcedure);


            response = result.Read<AllDesigationsResponseList>().FirstOrDefault()!;
            response.Data = result.Read<AllDesigationsList>().ToList();
            response.TotalRecords = result.Read<int>().FirstOrDefault();
            return response;

        }
    }
}
