﻿using Core.Interface;
using Core.Models;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static Core.Models.DesigationsListParameter;
using static Core.Models.DesignationListById;
using static Core.Models.EmployeeListParameter;

namespace NewCrudTask.Controllers
{
    [Route("api/designation")]
 
    public class DesignationController : ControllerBase
    {
        private readonly IDesignationRepository _designationRepository;
        public DesignationController(IDesignationRepository designationRepository)
        {
            _designationRepository = designationRepository;
        }

        private async Task<ActionResult<T>> HandleInvalidModelState<T>(Func<Task<T>> action)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage)),

                });
            }

            T response = await action();

            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }

        private async Task<ActionResult<T>> HandleException<T>(Func<Task<T>> action)
        {
            try
            {
                return await HandleInvalidModelState(action);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }



        [HttpPost]
        public async Task<ActionResult<Response>> InsertData([FromBody] DtoAddDesignation dtoAddDesignation)
        {
            return await HandleException(async () => await _designationRepository.AddAsync(dtoAddDesignation));
        }


        [HttpPut("id")]
        public async Task<ActionResult<Response>> UpdateData([FromBody] DtoUpdateDesignation dtoUpdateDesignation)
        {
            return await HandleException(async () => await _designationRepository.UpdateAsync(dtoUpdateDesignation));
        }

     


        [HttpGet("id")]
        public async Task<ActionResult<DesignationResponseList>> GetAllEmployeesListByDesignationAsync(DesignationListById designationListById)
        {
            return await HandleException(async () => await _designationRepository.GetAllEmployeesListByDesignationAsync(designationListById));
            //return Ok(result);
        }



        [HttpGet]
        public async Task<ActionResult<AllDesigationsResponseList>> GetAllData(DesigationsListParameter desigationsListParameter)
        {
            return await HandleException(async () => await _designationRepository.GetAllDesignationsListAsync(desigationsListParameter));
            //return Ok(result);
        }



    }
}
