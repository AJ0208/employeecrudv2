﻿ALTER ROLE [db_owner] ADD MEMBER [anjali];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [anjali];


GO
ALTER ROLE [db_securityadmin] ADD MEMBER [anjali];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [anjali];


GO
ALTER ROLE [db_backupoperator] ADD MEMBER [anjali];


GO
ALTER ROLE [db_datareader] ADD MEMBER [anjali];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [anjali];


GO
ALTER ROLE [db_denydatareader] ADD MEMBER [anjali];


GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [anjali];

