﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateEmployeeData]
(
	@Id               UNIQUEIDENTIFIER,
	@Name             VARCHAR(100),
	@DesignationID    UNIQUEIDENTIFIER,
	@Salary           MONEY,
	@City             VARCHAR(100)
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @Name = TRIM(@Name);
    SET @City = TRIM(@City );

	IF EXISTS (SELECT 1 FROM [dbo].[tblEmployees] WITH (NOLOCK) 
	WHERE ID = @ID AND Active = 1)

	     BEGIN
		       UPDATE [dbo].[tblEmployees]
			   SET
			   [Name]         =  @Name,
			   DesignationID  =  @DesignationID,
			   Salary         =  @Salary,
			   City           =  @City ,
			   UpdatedOn      =  GETUTCDATE(),
			   UpdatedBy      =  NEWID()
			   WHERE
			   Id  = @Id;
			   
			  SELECT 'Employee data have been updated successfully' AS 'Message'
		  END
	ELSE
	      BEGIN
		       SELECT 'Employee does not exists please try again' AS 'Message'
		  END
 END
