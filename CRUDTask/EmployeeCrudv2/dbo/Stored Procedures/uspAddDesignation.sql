﻿CREATE PROCEDURE [dbo].[uspAddDesignation]
(
      @Name  VARCHAR(100)
)	
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @Name = TRIM(@Name);

	IF NOT EXISTS (SELECT 1 FROM [dbo].[tblDesignations] WITH (NOLOCK) 
	WHERE Name = @Name )

	   BEGIN
	        INSERT INTO [dbo].[tblDesignations]
	        ( [Name])
	        VALUES
			( @Name)

	        SELECT 'Designation has been inserted successfully!' AS 'Message'
	   END
   Else
      BEGIN 
	       SELECT @Name + 'Designation already exists' AS 'Message'
	  END
END
