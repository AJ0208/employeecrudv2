﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllEmployeesList]
(
    @Start       INT          =  0,
    @PageSize    INT          = - 1,
    @SortCol     VARCHAR(100) =  NULL,
    @SearchKey   VARCHAR(500) =  ''    
)
AS
BEGIN
begin try
   
    SET NOCOUNT ON;

    SET @SortCol   =  TRIM(ISNULL(@SortCol, ''));
    SET @SearchKey =  TRIM(ISNULL(@SearchKey, ''));

    IF ISNULL(@Start, 0)     = 0 SET  @Start    = 0
    IF ISNULL(@PageSize, 0) <= 0 SET  @PageSize = - 1

    SELECT 1 AS [Status],'Success' AS [Message]

    SELECT E.ID , E.[Name] 'EmployeeName' , D.[Name] 'DesignationName' , Salary , City ,
    CreatedOn , CreatedBy , UpdatedOn , UpdatedBy , DeletedOn , DeletedBy
    INTO #TEMP
    FROM [dbo].[tblEmployees] E WITH (NOLOCK)
    INNER JOIN [dbo].[tblDesignations] D
    ON E.DesignationID = D.ID

    WHERE(E.[Name] LIKE '%'+ @SearchKey+'%'  )

    ORDER BY CASE WHEN @SortCol = 'Name_asc' THEN  E.[Name]  END ASC,
             CASE WHEN @SortCol = 'Name_desc' THEN E.[Name]  END DESC  

    OFFSET @START ROW
    FETCH NEXT (CASE WHEN @PageSize = - 1 THEN (SELECT COUNT(1) FROM [dbo].[tblEmployees] [Total Records]) 
                                          ELSE  @PageSize END) ROWS ONLY

    SELECT * FROM #TEMP
    SELECT COUNT(*)
    FROM #TEMP

END TRY
BEGIN CATCH

        DECLARE @Msg VARCHAR(MAX) = Error_message();
        DECLARE @ErrorSeverity INT = Error_severity();
        DECLARE @ErrorState INT = Error_state();
        RAISERROR (@Msg,@ErrorSeverity ,@ErrorState );
        SELECT 0 AS [Status],@Msg AS [Message]

   END CATCH 
END