﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[uspAddEmployeeData]
(
      @Name             VARCHAR(100),
	  @DesignationId    UNIQUEIDENTIFIER,
	  @Salary           MONEY,
	  @City             VARCHAR(100),
	  @CreatedBy        UNIQUEIDENTIFIER
)	
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @Name = TRIM(@Name);
	SET @City = TRIM(@City );

	IF NOT EXISTS (SELECT 1 FROM [dbo].[tblEmployees] WITH (NOLOCK) WHERE [Name] = @Name AND ACTIVE = 1) 
		
	   BEGIN
		IF EXISTS (SELECT 1 FROM [dbo].[tblDesignations] WHERE Id = @DesignationId)
			BEGIN
				INSERT INTO [dbo].[tblEmployees] 
				([Name] , DesignationID , Salary , City , CreatedBy)
				VALUES
				(@Name , @DesignationId   , @Salary , @City , @CreatedBy)

				SELECT 'Employee details has been inserted successfully!' AS 'Message'
			END
			ELSE 
				BEGIN 
					SELECT 'Provide proper DesignationId' 'Message'
				END
	   END
   Else
      BEGIN 
	       SELECT 'Employee already exists' AS 'Message'
	  END
END

