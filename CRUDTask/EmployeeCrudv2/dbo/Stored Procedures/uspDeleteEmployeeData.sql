﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[uspDeleteEmployeeData]
(
	@Id UNIQUEIDENTIFIER 
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM [dbo].[tblEmployees] WITH (NOLOCK) WHERE ID = @ID AND ACTIVE = 1)
	
	   BEGIN
    	      UPDATE [dbo].[tblEmployees]
	          SET 
			  Active     =   0 ,
			  DeletedOn  =   GETUTCDATE() ,
			  DeletedBy  =   NEWID()
	          WHERE Id = @Id

	          SELECT 'Data have been deleted successfully!' AS 'Message'
	    END

	ELSE

	    BEGIN	
	          SELECT 'Given employee value does not exists please check and try again!' AS 'Message'
	    END
END
