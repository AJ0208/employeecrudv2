﻿CREATE TABLE [dbo].[tblEmployees] (
    [Id]            UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]          VARCHAR (100)    NOT NULL,
    [DesignationID] UNIQUEIDENTIFIER NOT NULL,
    [Salary]        MONEY            NULL,
    [City]          VARCHAR (100)    NOT NULL,
    [CreatedOn]     DATETIME         NOT NULL,
    [CreatedBy]     UNIQUEIDENTIFIER NULL,
    [UpdatedOn]     DATETIME         NULL,
    [UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [DeletedOn]     DATETIME         NULL,
    [DeletedBy]     UNIQUEIDENTIFIER NULL,
    [Active]        BIT              DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Designation] FOREIGN KEY ([DesignationID]) REFERENCES [dbo].[tblDesignations] ([Id])
);

