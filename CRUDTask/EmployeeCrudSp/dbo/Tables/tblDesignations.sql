﻿CREATE TABLE [dbo].[tblDesignations] (
    [Id]   UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name] VARCHAR (100)    NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

