﻿CREATE TABLE [dbo].[tblEmployees] (
    [Id]            UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Name]          VARCHAR (100)    NOT NULL,
    [DesignationId] UNIQUEIDENTIFIER NOT NULL,
    [Salary]        MONEY            NULL,
    [City]          VARCHAR (100)    NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tblEmployees_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]     UNIQUEIDENTIFIER NULL,
    [UpdatedOn]     DATETIME         NULL,
    [UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [DeletedOn]     DATETIME         NULL,
    [DeletedBy]     UNIQUEIDENTIFIER NULL,
    [Active]        BIT              DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([DesignationId]) REFERENCES [dbo].[tblDesignations] ([Id])
);

