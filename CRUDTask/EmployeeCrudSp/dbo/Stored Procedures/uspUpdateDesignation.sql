﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [uspUpdateDesignation]
(
	@Id       UNIQUEIDENTIFIER,
	@Name     VARCHAR(100)
)
AS
BEGIN	
	SET NOCOUNT ON;
	SET @Name = TRIM(@Name);
   
	IF EXISTS (SELECT 1 FROM [tblDesignations] WITH (NOLOCK) 
	WHERE Id = @Id )

	     BEGIN
		       UPDATE [tblDesignations]
			   SET
			   Id     = id,
			   [Name] =  @Name			  
			   WHERE
			   Id  = @Id ;			   
			  SELECT 'Designation have been updated successfully' AS 'Message'
		  END
	ELSE
	      BEGIN
		       SELECT 'Designation does not exists please try again' AS 'Message'
		  END
 END

