﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[uspGetEmployeeListById]
(
    @Id   VARCHAR(50)  =  NULL
)
AS
BEGIN
begin try
    SET NOCOUNT ON;

    SELECT 1 AS [Status],'Success' AS [Message]

    DECLARE @TotalRecords BIGINT

    SELECT E.Id , E.[Name] 'EmployeeName' , D.[Name] 'DesignationName' , Salary , City ,
    CreatedOn , CreatedBy , UpdatedOn , UpdatedBy , DeletedOn , DeletedBy
    FROM [dbo].[tblEmployees] E WITH (NOLOCK)
    INNER JOIN [dbo].[tblDesignations] D
    ON E.DesignationId = D.Id

    AND (ISNULL(@Id ,'') ='' or E.Id = @Id)


SELECT COUNT(*) FROM [dbo].[tblEmployees]

END TRY
BEGIN CATCH

        DECLARE @Msg VARCHAR(MAX) = Error_message();
        DECLARE @ErrorSeverity INT = Error_severity();
        DECLARE @ErrorState INT = Error_state();
        RAISERROR (@Msg,@ErrorSeverity ,@ErrorState );
        SELECT 0 AS [Status],@Msg AS [Message]

   END CATCH 
END
