﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [dbo].[uspUpdateEmployeeData]
(
	@Id               UNIQUEIDENTIFIER,
	@Name             VARCHAR(100),
	@DesignationId    UNIQUEIDENTIFIER,
	@Salary           MONEY,
	@City             VARCHAR(100),
	@UpdatedBy        UNIQUEIDENTIFIER
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @Name = TRIM(@Name);
    SET @City = TRIM(@City );

	IF EXISTS (SELECT 1 FROM [tblEmployees] WITH (NOLOCK) 
	WHERE Id = @Id AND Active = 1)

	     BEGIN
		       UPDATE [tblEmployees]
			   SET
			   [Name]         =  @Name,
			   DesignationId  =  @DesignationId,
			   Salary         =  @Salary,
			   City           =  @City ,
			   UpdatedOn      =  GETUTCDATE(),
			   UpdatedBy      =  @UpdatedBy
			   WHERE
			   Id  = @Id ;
			   
			  SELECT 'Employee data have been updated successfully' AS 'Message'
		  END
	ELSE
	      BEGIN
		       SELECT 'Employee does not exists please try again' AS 'Message'
		  END
 END

